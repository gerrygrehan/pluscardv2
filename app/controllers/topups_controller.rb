class TopupsController < ApplicationController
    before_action :logged_in_user, only: [:create, :new]  # Not sure if it should be create or new.
    before_action :correct_user,   only: [:create, :new] 
    
    def create
    @user = User.find(params[:id])

    @topup = Topup.create(user_id: params[:id], amount: params[:topup][:amount])  
  

      if @topup.save
        redirect_to @user
      else
        flash[:danger] = "Please ensure you enter an amount greater than €10 and less than €1,000"
        redirect_to @user  # This has to change to users page I think but can be done later. 
      end
    end
    

    def destroy
    end
    
    
    private

      def topup_params
        params.require(:topup).permit(:amount, :user_id, :status)
      end
      
end
