class Topup < ActiveRecord::Base
  belongs_to :user
  default_scope -> { order(created_at: :desc) }  # This will mean they are show in decending order.
  validates :user_id, presence: true                       
  validates :amount, presence: true, :inclusion => 10..1000
#  before_save { validate :upper_limit }
#  before_save { validate :lower_limit }
  def finish
    self.status = 2
  end

  private
  
#    def upper_limit
#      if (:amount >= 1000)
#        errors.add(amount, "is too high, the maximum you can top by is €1000")
#      end
#    end
        
#    def lower_limit
#      if (:amount <= 10)
#        errors.add(amount, "is too low, the mimimum you can top by is €10")
#      end
#    end

end

