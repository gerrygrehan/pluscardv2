class CreatePluscards < ActiveRecord::Migration
  def change
    create_table :pluscards do |t|
      t.string :number
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
    add_index :pluscards, [:user_id, :created_at]
  end
end
