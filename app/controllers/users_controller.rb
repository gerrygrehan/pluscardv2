class UsersController < ApplicationController
  before_action :logged_in_user, only: [:edit, :update] # this means a user must be logged in to do this. 
  before_action :correct_user,   only: [:edit, :update, :show] # means it must be a correct user 
  
  def new
    if logged_in?
      redirect_to current_user   # This needs to be changed to be redirected to user page. user_url won't work 
    else
    @user = User.new
    end
  end
  
  def show
    @user = User.find(params[:id]) # This uses the id instance to find user and pass it to class variable on the show page!!
    @topups = @user.topups.paginate(page: params[:page])  # This is used to show a list of top ups.  
    @topup = Topup.create
    @pluscards = @user.pluscards.paginate(page: params[:page])
    @pluscard = Pluscard.create
    #debugger                       This is causing server to crash
  end
  
  def create
    if logged_in?
      redirect_to current_user  
      else
      @user = User.new(user_params) 
      if @user.save
        log_in @user                                  # This will log_in the user automatically when they sign up
        flash[:success] = "Woohoo You are signed up!"  # Check this is working needs an id and some css
        redirect_to @user           # Could have used either   redirect_to user_url(@user) 
      else
        render 'new'
      end
    end
  end
  
  def edit
    @user = User.find(params[:id])
  end
  
  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      flash[:success] = "Profile updated"
      redirect_to @user
    else
      render 'edit'
    end
  end
  
  private 
  def user_params
    params.require(:user).permit(:name, :address1, :address2, :address3, :address4, :phone, :dob, :email, :password, :password_confirmation)
  end
  
  # Before filters
    # Confirms the correct user.
    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless @user == current_user
    end
  
end
