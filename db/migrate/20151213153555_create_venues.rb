class CreateVenues < ActiveRecord::Migration
  def change
    create_table :venues do |t|
      t.string :picture
      t.string :name
      t.text :description
      t.decimal :price, :precision => 6, :scale => 2

      t.timestamps null: false
    end
  end
end
