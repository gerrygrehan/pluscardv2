class Pluscard < ActiveRecord::Base
  belongs_to :user
  default_scope -> { order(created_at: :desc) }
  validates :user_id, presence: true
  validates :number, presence: true, length: { maximum: 16 }
end
