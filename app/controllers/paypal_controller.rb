class PaypalController < ApplicationController
    def validate
    params.permit!
    
    @user = User.find(current_user.id)
    
    @topup = Topup.find(params[:item_number])
    
 #   topupAmount = params[:mc_gross]    # get the amount 
        if ((params[:payment_status] == "Completed") || (params[:pending_reason] == "multi_currency"))          #   payer staus is what paypal sends pack.
    
        update_balance
        update_status
    
        flash[:success] = "Thanks for your top up"
        
        redirect_to current_user
    else
        flash[:danger] = "Sorry something went wrong and your account has not been updated."
        
        redirect_to current_user
        
        end
    end
    
    def handle
        # might need to put in if status for update or failed top up. 
        
        redirect_to current_user
        
    end
  
  private
   
    
    def update_balance
        topupAmount = (params[:mc_gross]).to_i 
        @user.update_attribute(:balance, (current_user.balance + topupAmount)) 
    end
    
    def update_status
        @topup.update_attribute(:status, "1")
    end
  
    
end
