class AddStatusToTopups < ActiveRecord::Migration
  def change
    add_column :topups, :status, :integer, :default => 0
  end
end
