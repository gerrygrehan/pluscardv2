class PurchasesController < ApplicationController
    before_action :logged_in_user, only: [:create, :new]  # Not sure if it should be create or new.
    before_action :correct_user,   only: [:create, :new] 
    before_action :check_balance, only: [:create]
    
    
    def create
    @user = User.find(params[:id])

    @purchase = Purchase.create(user_id: params[:id], venue_id: params[:venue_id], venue_price: params[:venue_price])  
  
    @venue = Venue.find(params[:venue_id])
    
    if check_balance
      flash[:danger] = "Please Top Up Your Account!"
      redirect_to @user
    else
      if @purchase.save
        flash[:success] = "Purchased!"
        reduce_balance
        
        redirect_to @user
      else
        flash[:danger] = "Not Purchased"
        redirect_to @user  # This has to change to users page I think but can be done later. 
      end
    end
    end

    def destroy
    end
    
    
    private

      def purchase_params
        params.require(:purchase).permit(:venue_id, :user_id, :venue_price)
      end
      
      def reduce_balance
        @user.update_attribute(:balance, (current_user.balance - @venue.price.to_i))
      end
      
      
      def check_balance
      @user = User.find(params[:id])
      @venue = Venue.find(params[:venue_id])
      @user.balance < @venue.price
         
      end
     
end