// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require_tree .



// This is the jquery for the forms. It just loads them nicely.


function showBalance() {
		{
		$("#balance").toggle();
		} 
}

function showTopupsTable() {
		{
		$("#display-topups").toggle("slow");
		} 
}

function showPluscardsTable() {
		{
		$("#display-pluscards").toggle("slow");
		} 
}

function ordercard() {
    alert("Thanks, we'll send your card to your address!!");
}

function ordervenue() {
    alert("Thanks for purchasing!!");
}


$(window).load(function() { //start after HTML, images have loaded
 
    var InfiniteRotator =
    {
        init: function()
        {
            //initial fade-in time (in milliseconds)
            var initialFadeIn = 250;
 
            //interval between items (in milliseconds)
            var itemInterval = 5000;
 
            //cross-fade time (in milliseconds)
            var fadeTime = 400;
 
            //count number of items
            var numberOfItems = $('.rotating-item').length;
 
            //set current item
            var currentItem = 0;
 
            //show first item
            $('.rotating-item').eq(currentItem).fadeIn(initialFadeIn);
 
            //loop through the items
            var infiniteLoop = setInterval(function(){
                $('.rotating-item').eq(currentItem).fadeOut(fadeTime);
 
                if(currentItem == numberOfItems -1){
                    currentItem = 0;
                }else{
                    currentItem++;
                }
                $('.rotating-item').eq(currentItem).fadeIn(fadeTime);
 
            }, itemInterval);
        }
    };
 
    InfiniteRotator.init();
 
});


// This is the jquery for the forms. It just loads them nicely.
$(window).load(function() {
	$(".form-body").slideUp(1).delay(200).slideDown('slow');
});


// This would validate log in if called, however it is currently not being called as rails is doing this. 
function validateLogin() {
	 var email = document.forms["loginForm"]["email"].value;
	 var pword = document.forms["loginForm"]["password"].value;
	
	if (email == "") {
		document.forms["loginForm"]["email"].style.backgroundColor = "red";
		document.forms["loginForm"]["email"].value = "Please Enter a value";
		alert("Please enter an Email");
	}
	
	if (pword == "") {
		document.forms["loginForm"]["password"].style.backgroundColor = "red";
		document.forms["loginForm"]["password"].value = "Please Enter a value";
		alert("Please enter a Password");
	}
}


////////////////////contact form js///////////////////
function checkMail(){	

	

	

		//Get values from the input fields

	var myMail = document.getElementById("email").value;

	var username = document.getElementById("username").value;

	var myText = document.getElementById("comment").value;

  	//check first name last and email 

  	if( username == ""){

  		

  		  alert("Your username can not be empty !");

  		  //display pre-made paragraph

  	

  		  return false;

  		  

  	}else if( myMail == ""){

  		

  		  alert("Your email can not be empty !");




  		  return false;

  		

  	}else if (myText.length == "" || myText.length > 1000 ){

  		

  		//alert("You have too many characters in you message. You should reduct them.");

  		  document.getElementById("textareaNotice").style.display = "block"; 

  	}

  		

  	else{

  		

  		alert("Thank you \n"+ username+" \n"+myMail+" \n"+myText);

		return true;

  	

  		//submit form and return a message

    
  	}

	

	}
	////////////////////contact form js end///////////////////
	
function showPaypal() {
		{
		$("#paypal").toggle("slow");
		} 
}	
	
$("showPaypal").bind("click", function(){
    $("#paypal").toggle("slow");
});