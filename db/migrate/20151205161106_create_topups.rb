class CreateTopups < ActiveRecord::Migration
  def change
    create_table :topups do |t|
      t.integer :amount
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
    add_index :topups, [:user_id, :created_at]
  end
end
