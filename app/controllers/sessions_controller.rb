class SessionsController < ApplicationController

  def new
    if logged_in?
      redirect_to current_user   # This needs to be changed to be redirected to user page. user_url won't work 
      else
      render 'new'
    end
  end

  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])      # this is creating the new session on log in
       log_in user                                                # Log the user in and redirect to the user's show page.
       params[:session][:remember_me] == '1' ? remember(user) : forget(user)   # Used for the remember me function
       redirect_back_or user                                                             
    else
      flash.now[:danger] = 'Invalid email/password combination' # Not quite right!
      render 'new'
    end
  end

  def destroy
    log_out if logged_in?
    redirect_to root_url
  end
end