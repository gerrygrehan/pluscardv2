class AddVenuePriceToPurchases < ActiveRecord::Migration
  def change
    add_column :purchases, :venue_price, :decimal, :precision => 6, :scale => 2
  end
end
