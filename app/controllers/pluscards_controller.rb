class PluscardsController < ApplicationController
    before_action :logged_in_user, only: [:create, :destroy]
    before_action :correct_user,   only: [:create, :new] 

    def create
        
    @user = User.find(params[:id])
    #@pluscard = current_user.pluscards.build(pluscard_params)
    @pluscard = Pluscard.create(user_id: params[:id], number: params[:pluscard][:number])
        if @pluscard.save
            flash[:success] = "Pluscard Registered!"
            redirect_to @user
        else
            flash[:danger] = "Something Went Wrong Please Try Again!!"
            
            redirect_to @user  # This has to change to users page I think but can be done later. 
        end
    end

    def destroy
        @pluscard = Pluscard.find(params[:id])
        
        @pluscard.destroy
        flash[:success] = "Pluscard Deleted"
        redirect_to root_url
    
    end
    
    
    private
        def pluscard_params
          params.require(:pluscard).permit(:number, :user_id)
        end
    
end
