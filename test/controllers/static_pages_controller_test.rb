require 'test_helper'

class StaticPagesControllerTest < ActionController::TestCase
  test "should get home" do
    get :home
    assert_response :success
    assert_select "Home", "PlusCard Cardless Payment System"
  end

  test "should get about" do
    get :about
    assert_response :success
    assert_select "About", "About | PlusCard Cardless Payment System"
  end

  test "should get terms" do
    get :terms
    assert_response :success
    assert_select "Terms", "Terms & Conditions | PlusCard Cardless Payment System"
  end

end
