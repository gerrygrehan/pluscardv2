User.create!(name:  "Gerry",
             address1: "Mullingar",
             phone: "123456",
             email: "grehangerry@gmail.com",
             password:              "gerrygerry",
             password_confirmation: "gerrygerry",
             admin: true)
             
User.create!(name:  "Liam",
             address1: "Navan",
             phone: "123456",
             email: "liam@liam.ie",
             password:              "liamliam",
             password_confirmation: "liamliam")             
