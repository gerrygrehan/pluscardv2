class AddBalanceToUsers < ActiveRecord::Migration
  def change
    add_column :users, :balance, :decimal, :precision => 6, :scale => 2, :default => 0.00
  end
end
