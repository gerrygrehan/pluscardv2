json.array!(@venues) do |venue|
  json.extract! venue, :id, :picture, :name, :description, :price
  json.url venue_url(venue, format: :json)
end
