class User < ActiveRecord::Base
  attr_accessor :remember_token, :activation_token, :reset_token  # Used for remember me function and activation
  before_save   :downcase_email                     # all this is for account activation 
  before_create :create_activation_digest
  
  
  before_save { self.email = email.downcase }
  validates :name,      presence: true, length: { maximum: 50 }
  validates :address1,  presence: true, length: { maximum: 255 }  
  validates :phone,     presence: true, length: { maximum: 20 }   # This Block is used to register users
#  validates :dob,       presence: true                           # Commented out for begining can be changed after.
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email,     presence: true, length: { maximum: 255 },  
                        format: { with: VALID_EMAIL_REGEX },
                        uniqueness: { case_sensitive: false }
  has_secure_password 
  validates :password, presence: true, length: { minimum: 6 }
  
  has_many :topups, dependent: :destroy
  has_many :pluscards, dependent: :destroy
  has_many :purchases, dependent: :destroy
  
  def User.digest(string)                                                         # This is here as part of testing that currently is not being use
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :      # But I think I will use it further down the line during cookies
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end
  
  # Returns a random token. This is used for the cookies for customers to use the remember me function. 
  def User.new_token
    SecureRandom.urlsafe_base64
  end
  
  # Here is the remeber me function. Remembers a user in the database for use in persistent sessions.
  def remember
    self.remember_token = User.new_token
    update_attribute(:remember_digest, User.digest(remember_token))
  end
  
  # Checks the cookie to make sure it matches users stored hash. Returns true if the given token matches the digest.
  def authenticated?(remember_token)
    return false if remember_digest.nil?
    BCrypt::Password.new(remember_digest).is_password?(remember_token)
  end
  
  # Makes it possible for a user to not be remembered
  def forget
    update_attribute(:remember_digest, nil)
  end
  
  # Sets the password reset attributes.
  def create_reset_digest
    self.reset_token = User.new_token
    update_attribute(:reset_digest,  User.digest(reset_token))
    update_attribute(:reset_sent_at, Time.zone.now)
  end

  # Sends password reset email.
  def send_password_reset_email
    UserMailer.password_reset(self).deliver_now
  end
  
  # Returns true if a password reset has expired.
  def password_reset_expired?
    reset_sent_at < 2.hours.ago
  end
  
  
  private
  
  # Converts email to all lower-case.
    def downcase_email
      self.email = email.downcase
    end

    # Creates and assigns the activation token and digest.
    def create_activation_digest
      self.activation_token  = User.new_token
      self.activation_digest = User.digest(activation_token)
    end
  
end
