class Purchase < ActiveRecord::Base
  belongs_to :user
  belongs_to :venue
  default_scope -> { order(created_at: :desc) }  # This will mean they are show in decending order.
  
  validates :user_id, presence: true
  validates :venue_id, presence: true
  validates :venue_price, presence: true
  
end
